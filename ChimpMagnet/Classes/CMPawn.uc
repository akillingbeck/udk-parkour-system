class CMPawn extends Pawn;



var byte currentCharge;
enum ECMMagneticCharge
{

	CMCharge_Negative,
	CMCharge_Neutral ,
	CMCharge_Positive ,

};

var CanvasIcon bananaTexture;
	
var name PreviousStateName;
var name HandVaultingState;
var name LedgeClimingState;

var bool bMonkeyBarOverhead,bMagnetLadderInfront,bBoolObjectAboveHead;

var PhysicsVolume currentPhysicsVol;
var Actor magnetInteractiveSide;
	
var bool lookingAtMagneticSide;
var ECMMagneticCharge currentArmMagneticCharge;
var SkeletalMeshComponent ArmsMesh[2]; 
/** Third person camera offset */
var vector CamOffset;
/** used to smoothly adjust camera z offset in third person */
var float CameraZOffset;

var bool bFoundEdge;

var bool tracedSomething;
var CMMonkeyBarVolume OnMonkeyBar;

var Actor actorCurrentlyTouching,lastActorTouched;

var float floorZ;//The Z position of the "Feet"

/*********************************************************************************************
 Camera related properties
********************************************************************************************* */
var Rotator cameraRotation;
var bool bFirstPerson;
var vector 	FixedViewLoc;
var rotator FixedViewRot;
var float CameraScale, CurrentCameraScale; /** multiplier to default camera distance */
var float CameraScaleMin, CameraScaleMax;

var CMPlayerProperties playerProperties;
/*FIX? MOVE THIS INPUT STUFF TO AN INPUT CLASS?*/
/*********************************************************************************************
 Input related properties
********************************************************************************************* */

var float lastTimeJumpPressed;
var bool bNegChargePressed,bPosChargePressed;

var float timeHeldDown;
var bool bChargingJump;
var() float holdJumpOffset;

/*********************************************************************************************
WallJump related
********************************************************************************************* */
var bool bCanWallJump;
var bool bReadyToWallJump;
var bool bJustWallJumped;
var vector wallJumpHitNormal;
var float wallJumpEnergy;


var bool bIsGrounded;

/*********************************************************************************************
Misc
********************************************************************************************* */
var bool bAttachedMonkeyBars;


var float healthRechargeTime;
var vector newWallStartPosition,newWallDest;
var bool newWallFound;
var int traceIterationsCaused,wallTraceIterationsCaused;
var bool bIsSprinting;
var bool bCanVaultObstacle;
var Actor tmp;
var CMMagnetSide currentTracedMagneticActor;

var vector pawnRotation,movementTraceStart,movementTraceDest,kneeHeightOffset,chestHeightOffset,headHeightOffset;
var float lowVaultHeight,medVaultHeight,highVaultHeight;
var vector bestEdgeLocation,previousTraceHitLocation;

var Vector2D screenCenter;
var vector magnetPickStart,magnetPickDirection;

/*****************************************
 Camera bob Related
 * ***************************************/
var float cameraSinTimer;
var float bobSpeed;
var float bobAmount;


/*****************************************
 Sound Related
 * ***************************************/
var AudioComponent WalkingSound;
var AudioComponent bananaMunch;
/*****************************************
 Score Related
 * ***************************************/
 var int bananaCount;
 var int chimpScore;

event PreBeginPlay()
{
	super.PreBeginPlay();
	
	
}
event PostBeginPlay()
{
	Super.PostBeginPlay();

	
	
	//SetCollisionType(COLLIDE_TouchAll);
	kneeHeightOffset.Z = -BaseEyeHeight * 2;//temporary, maybe location of mesh feet ?
	chestHeightOffset.Z = -BaseEyeHeight;
	headHeightOffset.Z = 6;
}

function TakeFallingDamage()
{
	local float EffectiveSpeed;

	if (Velocity.Z < -0.5 * MaxFallSpeed)
	{
		if ( Role == ROLE_Authority )
		{
			MakeNoise(1.0);
			if (Velocity.Z < -1 * MaxFallSpeed)
			{
				EffectiveSpeed = Velocity.Z;
				if (TouchingWaterVolume())
				{
					EffectiveSpeed += 50;
				}
				if (EffectiveSpeed < -1 * MaxFallSpeed)
				{
					TakeDamage(-100 * (EffectiveSpeed + MaxFallSpeed)/MaxFallSpeed, None, Location, vect(0,0,0), class'DmgType_Fell');
				}
				}
		}
	}
	else if (Velocity.Z < -1.4 * JumpZ)
		MakeNoise(0.5);
	else if ( Velocity.Z < -0.8 * JumpZ )
		MakeNoise(0.2);
}
function ClimbMagnetLadder(Actor InteractiveSide)
{
	magnetInteractiveSide=InteractiveSide;
	//`log("Climbing");
	
	//CMPlayerController(Controller).GotoState('LadderClimb');
	//CMPlayerController(Controller).PopState(true);
	if(GetStateName() != 'LadderClimb')
	{
		SetPhysics(PHYS_Flying);
		CMPlayerController(Controller).PushState('LadderClimb');
	}
}
function StopMagnetLadder()
{
	magnetInteractiveSide=None;
	if ( Physics == PHYS_Flying )
	{
		SetPhysics(PHYS_Falling);
		//CMPlayerController(Controller).GotoState('PlayerWalking');
		CMPlayerController(Controller).PopState();
	}
}
function SwingMonkeyBars(Actor InteractiveSide)
{

	magnetInteractiveSide=InteractiveSide;
	//currentPhysicsVol=volume;

	if(GetStateName() != 'MonkeySwing')
	{
	SetPhysics(PHYS_Flying);
	//CMPlayerController(Controller).PopState(true);
	CMPlayerController(Controller).PushState('MonkeySwing');
	bAttachedMonkeyBars=true;
	}
	//if ( IsHumanControlled() )
		//Controller.GotoState('PlayerClimbing');
}
function bool CanGrabMonkeyBar()
{
	return ( bCanClimbLadders
			&& (Controller != None)
			&& (Physics != PHYS_Ladder)
			&& ((Physics != Phys_Falling) || (abs(Velocity.Z) <= JumpZ)) );
}
function EndClimbMonkeyBar(/*CMMonkeyBarVolume OldLadder*/)
{
	
	magnetInteractiveSide=None;
	if ( Physics == PHYS_Flying )
	{
		SetPhysics(PHYS_Falling);

		CMPlayerController(Controller).PopState();
		bAttachedMonkeyBars=false;
		//CMPlayerController(Controller).GotoState('PlayerWalking');
	}
}

/*********************************************************************************************
***********       MISC FUNCTIONS          **************************************************
********************************************************************************************* */

function ShootNeutralCharge()
{
	if(currentTracedMagneticActor != None)
	{
		currentTracedMagneticActor.changeCharge(1);
	}

}
function HealMonkey()
{
	`log("Healing");
		HealDamage(50,None,class'DmgType_Fell');
}
/*********************************************************************************************
***********       TICKED FUNCTIONS          **************************************************
********************************************************************************************* */
event Tick(float dDeltaTime)
{
	local vector offset,Rot;
	local Rotator CameraRot;
	local vector hitLocation,HitNorm,tracestart,traceend,offsettrace;
	local Actor tracedActorAbove,tracedActorInfront;


	healthRechargeTime+=dDeltaTime;

	CameraRot=cameraRotation;
	CameraRot.Pitch=0;

	offsettrace.Z=60;

	tracestart=Location;
	tracestart.Z=Location.Z + CylinderComponent.CollisionHeight/2;
	traceend=tracestart+offsettrace;

	offset.Z = 20;

	if(healthRechargeTime >= 5.0f)
	{
		healthRechargeTime=0;
		HealMonkey();
	}

	
	

	if(VSize(Velocity) > 0)
	{
		floorZ = (Location.Z - CylinderComponent.CollisionHeight); // Calculate new "foot level"
		if(Velocity.Z ==0)
		{
			if(VSize(Velocity) < 400)
			{
				bobAmount = 5;
				bobSpeed = 3;
			}
			else if(VSize(Velocity) > 400 && VSize(Velocity) < 700)
			{
				bobAmount = 5;
				bobSpeed = 5;
			}
			else
			{
				bobSpeed = 8;
				bobAmount=7;
			}
		}
		else
		{
			bobAmount=1;
			bobSpeed = 1;
		}
		//bobSpeed = VSize(Velocity) * 0.01;
	}
	else
	{
		bobAmount=1;
		bobSpeed = 1;
	}

	Mesh.SetRotation(CameraRot);
	obstacleTrace();

	
	pickMagnets();

	//Trace just above the head, used to determine if there is a monkey bar over your head(You have previously entered one)
	//and if there is, to continue until there isn't one 
	tracedActorAbove = Trace(hitLocation,HitNorm,tracestart,traceend,true);
	//DrawDebugLine(tracestart,traceend,0,0,255);
	if(tracedActorAbove!=None)
	{
		bBoolObjectAboveHead=true;
		if(tracedActorAbove.IsA('CMMonkeyBarVolume'))
		{
			//`log("Monkey");
			bMonkeyBarOverhead=true;
		}
		else
		{
			bMonkeyBarOverhead=false;
		}
	}
	else
	{
		bMonkeyBarOverhead=false;
		bBoolObjectAboveHead=false;
	}


	if(bAttachedMonkeyBars)
	{
		if(bMonkeyBarOverhead==false)
		{
			EndClimbMonkeyBar();
		}
	}


	tracestart=Location;
	traceend=tracestart+(vector(Rotation)*35);
	//DrawDebugLine(tracestart,traceend,0,255,0);

	tracedActorInfront=Trace(hitLocation,HitNorm,tracestart,traceend,true);
	if(tracedActorInfront!=None)
	{
		if(tracedActorInfront.IsA('CMMonkeyBarVolume'))
		{
			bMagnetLadderInfront=true;
			//`log("LADDER");
			//bMonkeyBarOverhead=true;
		}
		else
		{
			bMagnetLadderInfront=false;
			//bMonkeyBarOverhead=false;
		}
	}
	else
	{
		bMagnetLadderInfront=false;
	}


	//used for adding to jump height the longer you hold it down(adds velocity for max 1 seconds)
	if(bChargingJump && WorldInfo.TimeSeconds - lastTimeJumpPressed < 4.0f)
	{
		//arbitrary values 
		Velocity.Z+=holdJumpOffset*dDeltaTime;//300 UU per second?
	}

	if(VSize(Velocity)!=0 && CMPlayerController(Controller).bStoppedSprint && Velocity.Z == 0)
	{
		WalkingSound.Play();
		WalkingSound.VolumeMultiplier = 2;
	}
	else
	{
		WalkingSound.Stop();
	}

	



}
function GiveBanana()
{
	bananaCount++;
	chimpScore+=50;
	bananaMunch.Play();
}
event Touch(Actor other,PrimitiveComponent otherComp,Vector HotLoc,Vector HitNorm)
{
	//`log("TOUCHNING :"@other.GetDebugName());
	

	local vector ZAxis;
	ZAxis.Z=1;
	
	//actorCurrentlyTouching=other;
	//if(actorCurrentlyTouching.IsA('CMMonkeyBarVolume') && lastActorTouched.IsA('CMMonkeyBarVolume'))
	//{
	//	bTouchedAnotherInteractiveMagnetSide=true;
	//}

}
function DrawHud(HUD h)
{
	super.DrawHud(h);
	
	screenCenter.X=h.Canvas.SizeX/2;
	screenCenter.Y=h.Canvas.SizeY/2;
	
	h.Canvas.DeProject(screenCenter,magnetPickStart,magnetPickDirection);

	
	h.Canvas.DrawIcon(bananaTexture,0,0,0.5f);
	h.Canvas.DrawText(":"@bananaCount,,4,4);
	h.Canvas.DrawText("Health : "@Health,,4,4);
}
event UnTouch(Actor Other)
{
	
	local vector ZAxis;
	

	lastActorTouched=Other;

	//ZAxis.Z=1;
	//if(Other.IsA('CMMonkeyBarVolume') && !actorCurrentlyTouching.IsA('CMMonkeyBarVolume'))
	//{
	//	bTouchedAnotherInteractiveMagnetSide=false;
	//}
	//`log("STOPPED TOUCHING :"@Other.GetDebugName());
}
function bool InLowerRange()
{
	return ((bestEdgeLocation.Z > ((floorZ)))
		&& (bestEdgeLocation.Z < ((floorZ)+lowVaultHeight)))? true : false;
}
function bool InMiddleRange()
{
	return ((bestEdgeLocation.Z >= ((floorZ)+lowVaultHeight))
		&&(bestEdgeLocation.Z < ((floorZ)+medVaultHeight))) ? true : false;
}
function bool InUpperRange()
{
	return ((bestEdgeLocation.Z >= ((floorZ)+medVaultHeight))
		&&(bestEdgeLocation.Z < ((floorZ)+highVaultHeight))) ? true : false;
}
function obstacleTrace()
{
	local vector HitLoc,HitNormal;
	local vector Rot;
	local vector traceOffset;
	local bool possibleEdge;
	local vector newWallHitLoc,newWallHitNormal;
	possibleEdge=false;


		Rot=vector(Rotation);
		movementTraceStart=Location + (Rot*8);
		movementTraceStart.Z = floorZ;
		newWallStartPosition=movementTraceStart;


		movementTraceDest=Location + (Rot*96);
		movementTraceDest.Z = floorZ;
		newWallDest = movementTraceDest;

		traceOffset.Z = 4;
		tracedSomething=false;
		


			//if we have a velocity > 0, we are moving and should therefore start checking for traces and possible edges
		if(VSize(Velocity) > 0)
		{
			//If there is nothing infront of us, we shouldnt have an edge or hit location
			if(Trace(HitLoc,HitNormal,movementTraceDest,movementTraceStart,true)==None)
			{
				
				tracedSomething=false;
				
				previousTraceHitLocation.X=0;
				previousTraceHitLocation.Y=0;
				previousTraceHitLocation.Z=0;
				wallTraceIterationsCaused=0;
				
				//Because we want to be able to vault objects which do not start directly at our feet, we need to keep tracing until we discover one such object
				//This will run a max 30 times. It could be optimised like the other Recurse function to check at each level first to determine if there is a wall
				//at any of those and if not trace for the entirity.
				RecurseTraceUntilWall(newWallHitLoc,newWallHitNormal,newWallDest,newWallStartPosition,traceOffset,0);
				
			}
			else
			{
				wallTraceIterationsCaused=0;
				tracedSomething=true;
			}

			traceIterationsCaused=0;

			//newWallFound is set within the RecurseTraceUntilWall function. Which keeps tracing until there is a wall, once there is a wall this boolean gets set and we now know 
			//there will be a possible edge from an object which is "hanging" i.e. isn't located on the floor
			if(newWallFound==true)
			{
				RecurseTraceUntilEdge(HitLoc,HitNormal,newWallDest,newWallStartPosition,traceOffset,0);
			}
			else
			{
				RecurseTraceUntilEdge(HitLoc,HitNormal,movementTraceDest,movementTraceStart,traceOffset,0);
			}
		}
		else
		{
			wallTraceIterationsCaused=0;
			traceIterationsCaused=0;
		}
		//if bestEdgeLocation has a length greater than 0, tehre was an edge recorded
		if(VSize(bestEdgeLocation) != 0)
		{
			bFoundEdge=true;
		}
		else
		{
			bFoundEdge=false;
		}
		//debug stuff for showing the edge we have found
		if(InLowerRange())
		{
			//DrawDebugSphere(bestEdgeLocation,20,20,0,0,255);
		}
		else if(InMiddleRange())
		{
			//DrawDebugSphere(bestEdgeLocation,20,20,0,255,0);
		}
		else if(InUpperRange())
		{
			//DrawDebugSphere(bestEdgeLocation,20,20,255,0,0);
		}
		//DrawDebugLine(newWallStartPosition,newWallDest,0,0,255);
		//	DrawDebugLine(movementTraceStart,movementTraceDest,255,0,0);

}
//This function us exactly like the RecurseUntilEdge function with 1 fundemental difference. This continues until it finds a possible obstacle, so it traces through mid-air until it reaches
//a target. Be it an obstacle or the function call limit//
function RecurseTraceUntilWall(Vector HitLoc, vector HitNormal, vector TraceDest,vector TraceStart,vector traceOffset,int currentRecurse)
{
	local Actor other;
	local vector zero;
	local vector one;
	//if(Trace(
	other=Trace(HitLoc,HitNormal,TraceDest ,TraceStart ,true);

	wallTraceIterationsCaused++;
	if(wallTraceIterationsCaused > 30)
	{
		bestEdgeLocation=zero;
		newWallFound=false;
		return;
	}


		if(other == None)//TraceStart.Z < Location.Z + 234)
		{
			RecurseTraceUntilWall(HitLoc,HitNormal,TraceDest + traceOffset,TraceStart + traceOffset,traceOffset,currentRecurse++);
		}
		else
		{
			newWallFound=true;
			newWallStartPosition=TraceStart;
			newWallDest=TraceDest;
			return;
		}

	return;
}
function RecurseTraceUntilEdge(Vector HitLoc, vector HitNormal, vector TraceDest,vector TraceStart,vector traceOffset,int currentRecurse)
{
	local vector temp;
	temp.X=0;
	temp.Y=0;
	temp.Z=0;

	//akill08 
	/*WORST CASE SCENARIO IS THE RECURSIVE FUNCTION RUNS 49 TIMES(If it returns a trace) AND DOESN'T REACH "Empty Space"*/
	/*Which will happen if the wall/object is too large (such as a BSP wall with no edge)*/
	/*Change this to something more efficient if there is time, such as a bi-search system*/
	traceIterationsCaused++;
	//removing this will cause a call limit on the function and will crash UDK. DO NOT REMOVE!
	//(Number of iterations can be increased if you want to search for longer, change this in conjunction with the traceOffset)
	//traceOffset.z = 20...each iteration will offset by 20....increasing this number means you have a higher chance of reaching an empty destination before the limit
	// but it sacrifices precision of the edge.
	if(traceIterationsCaused > 50)
	{
		//`log("Edge Trace timed out");
		bestEdgeLocation=temp;
		return;
	}

	bestEdgeLocation=previousTraceHitLocation;
		if(Trace(HitLoc,HitNormal,TraceDest ,TraceStart ,true) != None)//TraceStart.Z < Location.Z + 234)
		{
			previousTraceHitLocation = HitLoc;
			RecurseTraceUntilEdge(HitLoc,HitNormal,TraceDest + traceOffset,TraceStart + traceOffset,traceOffset,currentRecurse++);
		}


		return;



}
function pickMagnets()
{
	local vector HitNormal,HitLocation;
	local Actor theActor;
	local TraceHitInfo hitInfo;
	local string temp;


	
	lookingAtMagneticSide=false;
	foreach TraceActors(class'Actor', theActor, HitLocation, 
                        HitNormal, magnetPickStart + magnetPickDirection*32768, magnetPickStart,,hitInfo) 
    {
		
		if(theActor!=None)
		{
			
			
			
			//if(hitInfo.Material != None)
			//{
			//	`log("YES! #1");
			//	if(hitInfo.Material.GetParameterDesc('magnetSide', temp))
			//	{
			//		`log("YES! #2");
			//		if(temp == "True")
			//		{
			//			lookingAtMagneticSide=true;
			//		}
			//		else
			//		{
			//			lookingAtMagneticSide=false;
			//		}
			//	}
			

			//	if(hitInfo.Material.GetParameterDesc('magnetSide', temp))
			//	{
			//		`log("YES!");
			//		if(temp == "True")
			//		{
			//			lookingAtMagneticSide=true;
			//		}
			//		else
			//		{
			//			lookingAtMagneticSide=false;
			//		}
			//	}
			//	//if(hitInfo.Material.GetPackageName() != '')
			//	//{
			//	//	`log("The material is "@hitInfo.Material.GetPackageName());
			//	//}
			////`log("The material is "@hitInfo.Material.GetPackageName());
			//}
			
			if(theActor.IsA('CMMagnetSide'))
			{
				
				
				//if(theActor == CMMagnet(theActor).defaultPrefab.PrefabArchetypes
				//theActor.ChildActors(class'StaticMesh
				//CMMagnet(theActor).defaultPrefab.
				//if(CMMagneticBox(theActor).isAMagnetSide)
				//{
					currentTracedMagneticActor = CMMagnetSide(theActor);
				//}
				//else
				//{
				//	currentTracedMagneticActor = None;
				//}
				break;
			}
			else
			{
				currentTracedMagneticActor = None;

			}

		}
		else
		{
			currentTracedMagneticActor = None;
		}

    }

}

/*********************************************************************************************
***********      DEBUG FUNCTIONS          **************************************************
********************************************************************************************* */
simulated function DisplayDebug(HUD Hud,out float out_YL, out float out_YPos)
{
	local String T;
	local Canvas canvas;

	canvas = Hud.Canvas;
	screenCenter.X=canvas.SizeX/2;
	screenCenter.Y=canvas.SizeY/2;

	canvas.DeProject(screenCenter,magnetPickStart,magnetPickDirection);

	out_YPos += out_YL;

	 Canvas.SetPos(4, out_YPos);
    Canvas.SetDrawColor(255,0,0);
 
    T = "Player [" $ GetDebugName() $ "]";

    Canvas.DrawText(T, FALSE);
	

	out_YPos += out_YL;

	Canvas.SetPos(4, out_YPos);
    Canvas.SetDrawColor(255,0,0);

	out_YPos += out_YL;
	Canvas.DrawText("Velocity:" @Velocity @" Acceleration: "@Acceleration);
	Canvas.SetPos(4, out_YPos);
	/*out_YPos += out_YL;
	Canvas.SetPos(4, out_YPos);
	Canvas.DrawText("EyeHeight:" @EyeHeight @" Base Eye Height "@BaseEyeHeight);*/
	/*out_YPos += out_YL;
	Canvas.SetPos(4, out_YPos);
	Canvas.DrawText("Charging Jump:" @bChargingJump);*/
	out_YPos += out_YL;
	Canvas.SetPos(4, out_YPos);
	Canvas.DrawText("Arm Charge" @currentArmMagneticCharge);
	out_YPos += out_YL;
	Canvas.SetPos(4, out_YPos);

	if(actorCurrentlyTouching!=None)
	{
	Canvas.DrawText("Currently Touching" @actorCurrentlyTouching.GetDebugName());
	}
	out_YPos += out_YL;
	Canvas.SetPos(4, out_YPos);
	if(lastActorTouched!=None)
	{
	Canvas.DrawText("Just stopped Touching" @lastActorTouched.GetDebugName());
	}

	if(currentTracedMagneticActor != None)
	{
		out_YPos += out_YL;
		Canvas.SetPos(4, out_YPos);
		Canvas.DrawText("Box [" $ currentTracedMagneticActor.GetDebugName() $ "]");
		out_YPos += out_YL;
		Canvas.SetPos(4, out_YPos);
		Canvas.DrawText("Box Charge: " @currentTracedMagneticActor.theCharge);


	}
	/*	out_YPos += out_YL;
	Canvas.SetPos(4, out_YPos);
	Canvas.DrawText("Tracing magnet part: " @lookingAtMagneticSide);*/
	/*out_YPos += out_YL;
	Canvas.SetPos(4, out_YPos);
	Canvas.DrawText("Trace Iterations" @traceIterationsCaused);*/
	/*	out_YPos += out_YL;
	Canvas.SetPos(4, out_YPos);
	Canvas.DrawText("Wall Trace Iterations" @wallTraceIterationsCaused);*/
			out_YPos += out_YL;
	Canvas.SetPos(4, out_YPos);
	Canvas.DrawText("Monkey bar above" @bMonkeyBarOverhead);
	out_YPos += out_YL;
	Canvas.SetPos(4, out_YPos);
	Canvas.DrawText("Ladder infront" @bMagnetLadderInfront);
		out_YPos += out_YL;
	Canvas.SetPos(4, out_YPos);
	Canvas.DrawText("CURRENT HEALTH " @Health);

	

	//out_YPos += out_YL;
	//Canvas.SetPos(4, out_YPos);
	//Canvas.DrawText("Wall jump bool" @bCanWallJump);

	//out_YPos += out_YL;
	//Canvas.SetPos(4, out_YPos);
	//Canvas.DrawText("Pawn Z position" @Location.Z);

	//	out_YPos += out_YL;
	//Canvas.SetPos(4, out_YPos);
	//Canvas.DrawText("Eye position" @Location.Z- CylinderComponent.CollisionHeight/2);

	//	out_YPos += out_YL;
	//Canvas.SetPos(4, out_YPos);
	//Canvas.DrawText("Left Turn" @CMPlayerController(PlayerController).leftTurn);

	
	

}
/*********************************************************************************************
***********       CUSTOM FUNCTIONS          **************************************************
********************************************************************************************* */
function int defaultAccelerate()
{
	return Default.AccelRate;
}
function Sprint()
{
	bIsSprinting=true;
	//if(VSize(Velocity)>=Default.GroundSpeed/2)
	//{
	GroundSpeed = 1024;
	AccelRate = 512;
	//}
}
function StopSprint()
{
	bIsSprinting=false;
	GroundSpeed=Default.GroundSpeed;
	AccelRate=Default.AccelRate;
	
}

function ShootPositiveCharge()
{
	bPosChargePressed=true;
	//currentTracedMagneticActor.changeDirection();
		if(bNegChargePressed)
		{
			ShootNeutralCharge();
		}
		else
		{
			currentTracedMagneticActor.changeCharge(2);
		}

}
function ReleasePositiveCharge()
{
	bPosChargePressed=false;
}
function ShootNegativeCharge()
{
	bNegChargePressed=true;
	//currentTracedMagneticActor.changeDirection();
		if(bPosChargePressed)
		{
			ShootNeutralCharge();
		}
		else
		{
			currentTracedMagneticActor.changeCharge(0);
		}
	

}
function ReleaseNegativeCharge()
{
	bNegChargePressed=false;
}
function ShootMagneticCharge()
{
	//currentTracedMagneticActor.changeDirection();
		currentTracedMagneticActor.changeCharge(int(currentArmMagneticCharge));
}
function PrevCharge()
{
	local int tempVal;
	tempVal = int(currentArmMagneticCharge);
	if(tempVal>0)
	{
		tempVal--;
	}
	currentArmMagneticCharge=ECMMagneticCharge(tempVal);

}
function NextCharge()
{
	local int tempVal;
	tempVal = int(currentArmMagneticCharge);
	if(tempVal<2)
	{
		tempVal++;
	}
	currentArmMagneticCharge=ECMMagneticCharge(tempVal);

}
function ToggleCamera()
{
	bFirstPerson  = !bFirstPerson;
}
/*********************************************************************************************
***********       CAMERA FUNCTIONS          **************************************************
********************************************************************************************* */
simulated function bool CalcCamera( float fDeltaTime, out vector out_CamLoc, out rotator out_CamRot, out float out_FOV )
{
	local vector HitNormal;
	local vector CamRot;
	local float Radius, Height;
	local vector offset;

	GetBoundingCylinder(Radius, Height);

	cameraSinTimer+=fDeltaTime;

	if(!bFirstPerson)
	{
		out_CamLoc = Location - vector(out_CamRot) * Radius * 5 * (1-fDeltaTime);
		out_CamRot = Rotation;

		offset.Z = 100;
		offset.Y = 0;
		//EyeHeight-=timeHeldDown*100;


		out_CamLoc += offset;

	cameraRotation=out_CamRot;
		return true;
	}
	else
	{
		//BaseEyeHeight-=timeHeldDown*100;
		out_CamLoc = GetPawnViewLocation();
		out_CamRot = GetViewRotation();

		//out_CamLoc  = 
		//GetActorEyesViewPoint(out_CamLoc, out_CamRot );
		cameraRotation=out_CamRot;
		return true;


	}
	
}

simulated event vector GetPawnViewLocation()
{
	local vector Socket_loc;
	local Rotator Socket_rot;
	local vector actualCamLocation;
	local vector bobOffset;

	bobOffset.Z = BaseEyeHeight + ( Sin(cameraSinTimer*bobSpeed)*bobAmount);


	if(Mesh.GetSocketWorldLocationAndRotation('Eye',Socket_loc,Socket_rot))
	{
		actualCamLocation = Socket_loc +bobOffset;// (BaseEyeHeight * bobOffset);

		return actualCamLocation;
	}
	return Location + BaseEyeHeight * vect(0,0,1);
}

simulated event rotator GetViewRotation()
{
	local vector Socket_loc;
	local Rotator Socket_rot;
	local Rotator result;
	if(Mesh.GetSocketWorldLocationAndRotation('Eye',Socket_loc,Socket_rot))
	{
		//`log("YE1");
		result=Controller.Rotation;
		//result.Pitch = rotator(Controller.GetFocalPoint() - Location).Pitch;
		return result;// + Socket_rot;
	}
	return Super.GetViewRotation();

}
/*********************************************************************************************
***********       MOVEMENT FUNCTIONS          **************************************************
********************************************************************************************* */

function ShrinkCollision()
{
	local vector newCamLoc;
	newCamLoc.Z = -10;
	`log("SHRINKING");
	//CylinderComponent.CollisionHeight = Default.CylinderComponent.CollisionHeight/2;
	CylinderComponent.SetCylinderSize(CylinderComponent.CollisionRadius,Default.CylinderComponent.CollisionHeight/2);
	//PlayerController(Controller).PlayerCamera.SetLocation(PlayerController(Controller).PlayerCamera.Location+newCamLoc);
	//Camera.SetLocation(camera.Location+newCamLoc);
}

function GrowCollision()
{
	local vector newCamLoc;
	newCamLoc.Z = 10;
	`log("GROWING");
	//CylinderComponent.CollisionHeight = Default.CylinderComponent.CollisionHeight;
	CylinderComponent.SetCylinderSize(CylinderComponent.CollisionRadius,Default.CylinderComponent.CollisionHeight);
	//PlayerController(Controller).PlayerCamera.SetLocation(PlayerController(Controller).PlayerCamera.Location+newCamLoc);
	//Camera.SetLocation(camera.Location+newCamLoc);
}
function bool canWallJump()
{
	return true;
}

function DoWallJump(bool bUpdating)
{
	local vector Rot;
	local vector jumpDirection;
	local float velocitySize;
	bJustWallJumped=true;
		//bReadyToWallJump=false;
		//bCanWallJump=false;
		wallJumpEnergy-=20;


	Rot = vector(Rotation);
	velocitySize=VSize(Velocity);

	jumpDirection = MirrorVectorByNormal(Rot,wallJumpHitNormal);
	Velocity = jumpDirection * velocitySize;
	Velocity.Z=0;
	Velocity.Z=JumpZ + (145 - (Default.wallJumpEnergy-wallJumpEnergy));
	//`log("Jump addition " @(145 - (Default.wallJumpEnergy-wallJumpEnergy)));
	//if(wallJumpHitNormal.X != 0)
	//{
		
	//Velocity.X = wallJumpHitNormal.X * (350);
	//}
	//if(wallJumpHitNormal.Y!=0)
	//	{
	//Velocity.Y = wallJumpHitNormal.Y * (350);
	//	}
	
	
}
event Landed(Vector HitNormal, Actor FloorActor)
{

	bIsGrounded=true;
	//Super.Landed(HitNormal,FloorActor);
	TakeFallingDamage();
	if ( Health > 0 )
		PlayLanded(Velocity.Z);
	LastHitBy = None;


	//`log("Landed and cant wallJump");
	wallJumpEnergy=Default.wallJumpEnergy;
	bChargingJump=false;
	bJustWallJumped=false;
	bReadyToWallJump=true;
	

}
event HitWall(Vector HitNormal,Actor Other,PrimitiveComponent OtherComp )
{
	//`log("Hit something, Physics is "@Physics);
	if(Physics == PHYS_Falling && bReadyToWallJump && CMPlayerController(Controller).GetStateName()!='QuickVault')
	{
		wallJumpHitNormal=HitNormal;
		//`log("Can wall Jump");
		bCanWallJump=true;
	}
	
}

function bool DoJump( bool bUpdating )
{
	if(Velocity.Z<=0)
	bChargingJump = true;

	JumpZ = Default.JumpZ;

	bIsGrounded=false;

		if (bJumpCapable && !bIsCrouched && !bWantsToCrouch && (Physics == PHYS_Walking || Physics == PHYS_Ladder || Physics == PHYS_Spider))
		{
			if ( Physics == PHYS_Spider )
				Velocity = JumpZ * Floor;
			else if ( Physics == PHYS_Ladder )
				Velocity.Z = 0;
			else if ( bIsWalking )
				Velocity.Z = Default.JumpZ;
			else
				Velocity.Z = JumpZ;
			if (Base != None && !Base.bWorldGeometry && Base.Velocity.Z > 0.f)
			{
				Velocity.Z += Base.Velocity.Z;
			}
			SetPhysics(PHYS_Falling);
			bCanWallJump=false;
			return true;
		}
	return false;

}

/*********************************************************************************************
***********       DEFAULT PROPERTIES          **************************************************
********************************************************************************************* */
DefaultProperties
{

	bananaTexture=(Texture=Texture2D'Chimp_Magnet.Textures.banana2')
	//Sound
	Begin Object class=AudioComponent name=WalkingAudioComponent
		SoundCue = SoundCue'Chimp_Magnet.Sound.Walking_Cue'
	End Object
	WalkingSound = WalkingAudioComponent
	
	Begin Object class=AudioComponent name=BananaMunchAudioComponent
			SoundCue = SoundCue'Chimp_Magnet.Sound.ChangeCharge_Cue'
	End Object
	bananaMunch = BananaMunchAudioComponent

	AirControl=+0.5
	AirSpeed=600
	
	chimpScore=0
	
	bananaCount=0

	bCanClimbLadders=true
	wallJumpEnergy=80

	healthRechargeTime=0

	bIsSprinting=false
	currentCharge=0
	
    bFirstPerson=true
	AccelRate=1024.0


	cameraSinTimer=0
	bobSpeed=1
	bobAmount=1

	bPushesRigidBodies=true
	Health=600
	HealthMax=600
	MaxFallSpeed=+800.0
	JumpZ=+000512.000000
	GroundSpeed=+00384.000000//256+128
	holdJumpOffset=+256


	bMagnetLadderInfront=false
	bMonkeyBarOverhead=false
	lookingAtMagneticSide=false
	bReadyToWallJump=true
	bJumpKeyIsUp=true
	bChargingJump=false
	bCanWallJump=false
	bDirectHitWall=true

	newWallFound=false
	bFoundEdge=false

	wallTraceIterationsCaused=0
	traceIterationsCaused=0

	HandVaultingState=HandVault
	LedgeClimingState=ClimbLedge

	lowVaultHeight=64 //eyeheight /2 (Centre of Collision Height)
	medVaultHeight=128 // below eyeheight
	highVaultHeight=256 //78 + 34

	tracedSomething=false

	

	Begin Object Name=CollisionCylinder
		CollisionRadius=+0034.000000
		CollisionHeight=+0064.000000
		BlockNonZeroExtent=true
		BlockZeroExtent=true
		BlockActors=true
		CollideActors=true
	End Object
	CollisionComponent=CollisionCylinder
	CylinderComponent=CollisionCylinder
	Components.Add(CollisionCylinder)
		//Begin Object class=SkeletalMeshComponent Name=SkeletalMeshComponent0
		//SkeletalMesh=SkeletalMesh'CH_IronGuard_Male.Mesh.SK_CH_IronGuard_MaleA'
		//bAcceptsLights=true
		//End Object
		//Mesh=SkeletalMeshComponent0
		//Components.Add(SkeletalMeshComponent0)
		begin object class=AnimNodeSequence name=blankanimseq 
		end object


		 Begin Object Class=SkeletalMeshComponent Name=FirstPersonArms
		SkeletalMesh=SkeletalMesh'AndyTest.PSKFiles.FirstPersonArms'
		AnimSets(0)=AnimSet'AndyTest.AnimSet.fparms_animset'
		AnimTreeTemplate=AnimTree'AndyTest.Anims.FirstPersonTree'
		Animations=blankanimseq
		DepthPriorityGroup=SDPG_Foreground
		FOV=55
		Scale3D=(X=0.5,Y=0.5,Z=0.5)
		Translation=(X=0,Y=0,Z=34)
		bAcceptsLights=true
		AbsoluteTranslation=false
		AbsoluteRotation=true
		AbsoluteScale=false
		TickGroup=TG_DuringASyncWork
	  End Object
	Mesh=FirstPersonArms
	Components.Add(FirstPersonArms)


	BaseEyeHeight=+000032.000000
	EyeHeight=+000000.000000
	currentArmMagneticCharge=CMCharge_Neutral

	//bCanMantle
}

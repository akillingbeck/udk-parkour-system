class CMBanana extends Actor placeable;

var(Nana) StaticMeshComponent StaticMeshComponent;
var MaterialInstanceConstant MatInst;




event PreBeginPlay()
{
	super.PreBeginPlay();

	//SetCollisionType(COLLIDE_TouchWeapons);
	
	CollisionComponent.SetAbsolute(false,false,false);
}


event PostBeginPlay()
{
	super.PostBeginPlay();

	if( StaticMeshComponent != none )
	{
		InitMaterialInstance();
	}


}
event Touch(Actor other,PrimitiveComponent otherComp,Vector HotLoc,Vector HitNorm)
{
	if(other != None)
	{
		if(other.isA('CMPawn'))
		{
			CMPawn(other).GiveBanana();
			Destroy();
		}
	}
}

/*simulated function DisplayDebug(HUD Hud,out float out_YL, out float out_YPos)
{
	local String T;
	local Canvas canvas;

	canvas = Hud.Canvas;

	out_YPos += out_YL;

	 Canvas.SetPos(4, out_YPos);
    Canvas.SetDrawColor(255,0,0);
 
    T = "Player [" $ GetDebugName() $ "]";

	if(currentAttractedActor!=None)
	{
			T= " It is: " $currentAttractedActor.GetDebugName();
	}
	else
	{
		T="";
	}
	 Canvas.DrawText(GetDebugName() $ ": Magnet has attractor: "@bHasAttractor $ T, FALSE);
	 out_YPos += out_YL;

	 Canvas.SetPos(4, out_YPos);
    Canvas.SetDrawColor(255,0,0);
	Canvas.DrawText(GetDebugName() $ ":current charge"@theCharge, FALSE);
 

	
	//if(currentAttracedSide!=None)
 //   Canvas.DrawText(GetDebugName() $ ": Current traced magnet: "@currentAttracedSide.GetDebugName(), FALSE);
	//else
	//Canvas.DrawText(GetDebugName() $ ": Dont have a magnet traced", FALSE);

}
*/

function InitMaterialInstance()
{
   MatInst = new(None) Class'MaterialInstanceConstant';
   MatInst.SetParent(StaticMeshComponent.GetMaterial(0));
   StaticMeshComponent.SetMaterial(0, MatInst);
   UpdateMaterialInstance();
}
function UpdateMaterialInstance()
{
	local LinearColor color;
	color.A=0;
	color.R=0;
	color.G=0;
	color.B=1;
   MatInst.SetVectorParameterValue('AmbientColor',color);
}


event Tick(float dTime)
{
super.Tick(dTime);

}

DefaultProperties
{


	


		Begin Object Class=DynamicLightEnvironmentComponent Name=MyLightEnvironment
			bEnabled=TRUE
		End Object
		LightEnvironment=MyLightEnvironment
		Components.Add(MyLightEnvironment)


		Begin Object Class=StaticMeshComponent Name=StaticMeshComponent0
		LightEnvironment=MyLightEnvironment
		bUsePrecomputedShadows=FALSE
		BlockNonZeroExtent=true
		BlockZeroExtent=true
		//BlockActors=false
		//CollideActors=true
		End Object
	bCollideActors=true
	bBlockActors=false
	CollisionComponent=StaticMeshComponent0
	//bCollideWorld=true
	StaticMeshComponent=StaticMeshComponent0

	Components.Add(StaticMeshComponent0)
}

class CMPlayerProperties extends Actor placeable;

var() float highestJumpOffset;
var() float jumpChargeSeconds;
var() vector cameraOffset;

DefaultProperties
{

	highestJumpOffset=+200
	jumpChargeSeconds=+1.0f
	cameraOffset=(0,0,100)
}
